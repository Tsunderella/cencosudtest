//
//  SourceTableViewController.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation
import UIKit

class SourcesTableViewController : UITableViewController {
    
    var sorting :String = "popular"
    private var webservice :Webservice!
    private var sourceListViewModel :SourceListViewModel!
    private var dataSource :TableViewDataSource<SourceTableViewCell,SourceViewModel>!
    var activityIndicator : UIActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        let rightButton = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(self.sortFunc))
        self.navigationItem.rightBarButtonItem = rightButton
        var indicator = UIActivityIndicatorView()
        titleHolder()
        configureActivityIndicator()
        updateUI(sorting: sorting)
    }
    func configureActivityIndicator(){
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.center = self.view.center
        self.view.addSubview(activityIndicator!)
    }
    @objc func sortFunc() {
       titleHolder()
        updateUI(sorting: sorting)
    }
    func titleHolder(){
        if sorting == "popular" {
            sorting = "rated"
            self.title = "Sorting by Rating"
        }else{
            sorting = "popular"
            self.title = "Sorting by Popularity"
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func updateUI(sorting: String) {
        if sorting == "popular" {
            
        }else{
            
        }
        self.webservice = Webservice()
        self.webservice.sorting = sorting
        self.sourceListViewModel = SourceListViewModel(webservice: self.webservice)
        
        // setting up the bindings
        self.sourceListViewModel.bindToSourceViewModels = {
            self.updateDataSource()
        }
        
    }
    
    private func updateDataSource() {
        print("updating datasource")
        self.dataSource = TableViewDataSource(cellIdentifier: Cells.source, items: self.sourceListViewModel.sourceViewModels) { cell, vm in
            print(vm.name)
            
            cell.nameLabel.text = vm.name
            cell.rate_value.text = String(describing: vm.rate!)
        }
        
        self.tableView.dataSource = self.dataSource
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentifier.showSourceDetails {
            performSegueForShowSourceDetails(segue: segue, sender: sender)
            
        }
    }
    
    
    private func performSegueForShowSourceDetails(segue :UIStoryboardSegue, sender: Any?) {
        
        guard let indexPath = self.tableView.indexPath(for: (sender as? UIButton)!) else {
            fatalError("indexPath not found")
        }
        
        let sourceViewModel = self.sourceListViewModel.source(at: indexPath.row)
        let sourceDetailsTVC = segue.destination as! SourceDetailsTableViewController
        sourceDetailsTVC.sourceViewModel = sourceViewModel
    }
}

extension UITableView {
    func indexPath(for view: UIView) -> IndexPath? {
        let location = view.convert(CGPoint.zero, to: self)
        return self.indexPathForRow(at: location)
    }
}

