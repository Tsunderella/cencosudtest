//
//  Webservice.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation

typealias JSONDictionary = [String:Any]

class Webservice {
    var sorting :String?
    private let sourcesURL = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=34738023d27013e6d1b995443764da44")!
    private let ratedURL = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=34738023d27013e6d1b995443764da44")!
    
    
    
    func loadSources(completion :@escaping ([Source]) -> ()) {
        var urlToLoad :URL?
        if sorting == "popular" {
            urlToLoad = ratedURL
        }else{
            urlToLoad = sourcesURL
        }
        
        URLSession.shared.dataTask(with: urlToLoad!) { data, _, _ in
            
            if let data = data {
                
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                let sourceDictionary = json as! JSONDictionary
                let dictionaries = sourceDictionary["results"] as! [JSONDictionary]
                //print(dictionaries)
                let sources = dictionaries.compactMap(Source.init)
                print(sources)
                DispatchQueue.main.async {
                    completion(sources)
                }
            }
            
            }.resume()
        
    }
    
}

