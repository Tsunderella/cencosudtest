//
//  Source.swift
//  CencosudTest
//
//  Created by Felipe Valdes on 2/16/19.
//  Copyright © 2019 Felipe Valdes. All rights reserved.
//


import Foundation

class Source {
    
    var id :NSNumber!
    var name :String!
    var description :String!
    var rate :NSNumber!
    var poster_url :String!
    
    init?(dictionary :JSONDictionary) {
        print(dictionary)
        guard let id = dictionary["id"] as? NSNumber,
            let name = dictionary["title"] as? String,
            let rate = dictionary["vote_average"] as? NSNumber,
            let poster_url = dictionary["poster_path"] as? String,
            let description = dictionary["overview"] as? String else {
                print("error")
                return nil
        }
        
        self.id = id
        self.name = name
        self.description = description
        self.rate = rate
        self.poster_url = poster_url
        print("rate is \(rate)")
    }
    
    init(viewModel :SourceViewModel) {
        
        self.id = viewModel.id
        self.name = viewModel.name
        self.description = viewModel.body
        self.rate = viewModel.rate
        self.poster_url = viewModel.poster_url
    }
    
}

